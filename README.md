# <center><font color="blue">Library Management Application with Symfony</font></center>

[[_TOC_]]

## 1- Description:

Cette application permet de gérer la bibliothèque de ISET SFAX.
<u>Elle permet, essentiellement:</u> 

- <u>à un utilisateur non authentifié de:</u>
  - accéder à la page d’accueil.
  - consulter la liste des livres disponibles dans la bibliothèque.
  - s'inscrire.
  - se connecter.
- <u>à un utilisateur authentifié de:</u>
  - accéder à la page d’accueil.
  - consulter la liste des livres disponibles dans la bibliothèque et avoir la possibilité de demander l'emprunt d'un livre et annuler cette demande avant la confirmation de l'administrateur de l'application (agent de la bibliothèque).
  - consulter la liste des livres empruntés.
  - consulter la liste des livres à emprunter (la liste des demandes d'emprunt).
  - créer un autre utilisateur.
  - se déconnecter.
- <u>à l’administrateur de:</u>
  - accéder à la page d’accueil.
  - consulter la liste des livres disponibles dans la bibliothèque et avoir la possibilité de les modifier, les supprimer et ajouter des nouveaux livres.
  - consulter la liste des éditeurs des livres et avoir la possibilité de les modifier, les supprimer et ajouter des nouveaux éditeurs.
  - consulter la liste des auteurs des livres et avoir la possibilité de les modifier, les supprimer et ajouter des nouveaux auteurs.
  - consulter la liste des catégories des livres et avoir la possibilité de les modifier, les supprimer et ajouter des nouveaux éditeurs.
  - consulter la liste des utilisateurs qui ont déjà empruntés ou ont demandé l'emprunt des livres et avoir la possibilité de confirmer la demande d'emprunt des livres et le retour des livres.
  - créer un autre utilisateur ou un autre administrateur.
  - se déconnecter.

## 2- Template:

La template est constitué d'un Menu vertical contenant le logo de l'ISET, le login de l'utilisateur authentifié et les pages disponibles selon l'état et le type d'utilisateur.

## 3- Authentification:

Les étudiants, les enseignants et les agents de la bibliothèque sont appelés à s'authentifier pour être éligible à exécuter des actions.
Chaque utilisateur non authentifié qui cherche à entrer à des pages qui nécessitent l'authentification sera rediriger automatiquement vers la page d'authentification avec l'url `/login`.

![Capture-01.png](/img/Capture-01.png)

Le blocage d’accès s'est fait à l'aide du code suivant dans le fichier `security.yaml`:
``` yaml
access_control:
        - { path: ^/user/, roles: [IS_AUTHENTICATED_FULLY, ROLE_USER] }
        - { path: ^/admin/, roles: 'ROLE_ADMIN' }
```

# 4- Inscription:

Chaque utilisateur peut d'inscrire à partir de la barre Menu `Inscription` avec l'url `/register`

![Capture - 20.png](/img/Capture-20.png)

Un utilisateur peut ajouter un autre utilisateur sans pouvoir modifier son rôle par défaut (USER: ROLE_USER).

![Capture - 19.png](/img/Capture-19.png)

Un utilisateur peut aussi ajouter un autre utilisateur en choisissant son rôle (soit ADMIN: ROLE_ADMIN soi USER: ROLE_USER).

![Capture - 21.png](/img/Capture-21.png)

Le choix du role s'est fait à travers la classe `RegistrationFormType.php`:
```php
class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('roles', ChoiceType::class, array('required' => false,
                'choices' => array('ADMIN' => 'ROLE_ADMIN', 'USER' => 'ROLE_USER'),'multiple' => true,))
            .......
    }
```

# 5- Choix d'implémentation des livres déja empruntés et des livres à emprunter (avec une demande d'emprunt):

Pour pouvoir stocker les livres empruntés et les livres qu'un utilisateur à demander de l'emprunter, on a ajouté deux attributs à l'entité User: `livre_empruntes` pour les livres empruntés et `livreToEmprunt` pour les livres en attente de confirmation d'emprunt.

Ces attributs décrit une relation `ManyToMany` entre l'entité `User` et l'entité `Livre`.
Deux tables intermédiaire se sont produites: une appelée par défaut `user_livre` et l'autre renommée `user_livreToEmprunt` pour éviter la confusion avec la première table intermédiaire. Le renommage de cette table s'est fait dans la classe Entity du User avec l'annotation:
```php
/**
     * @ORM\ManyToMany(targetEntity=Livre::class)
     * @ORM\JoinTable(name="user_livreToEmprunt")
     */
    private $livreToEmprunt;
```

# 6- Les pages sans authentification:

Sans authentification, un utilisateur peut accéder à la page d’`Accueil`, la page `Liste des livres` sans la colonne d'`Actions`, la page d'`Inscription` et la page de `Connexion`.

![Capture - 02.png](/img/Capture-02.png)

![Capture - 41.png](/img/Capture-41.png)

# 7- Les pages coté utilisateur avec un rôle USER:

Un utilisateur authentifié avec un rôle USER peut accéder aux pages ci-dessus:

![Capture - 42.png](/img/Capture-42.png)

![Capture - 43.png](/img/Capture-43.png)

![Capture - 44.png](/img/Capture-44.png)

![Capture - 45.png](/img/Capture-45.png)

Chaque utilisateur a le droit d'emprunter au maximum trois livres, donc le nombre total des livres déja empruntés et des livres qui a demandé à les emprunter ne doit pas dépasser trois.

Dans la page `Liste des livres`: 
- si le livre n'est pas emprunté et le nombre total des livres empruntés et des livres que l'utilisateur a demandé de les emprunter est inférieur à trois livres, un boutton bleu `Emprunter` est affiché.

![Capture - 47.png](/img/Capture-47.png)

- si le livre est déja emprunté, un bouton vert `confirmer` est affiché. (en cliquant un message d'alert est affiché `La demande d'emprunt de ce livre est confirmé.`)

![Capture - 46.png](/img/Capture-46.png)

- si l'emprunt du livre est en cours de confirmation de l'agent de la bibliothèque, un bouton rouge `Annuler` est affiché. (en cliquant l'utilisateur peut annuler la demande d'emprunt)

![Capture - 13.png](/img/Capture-13.png)

![Capture - 14.png](/img/Capture-14.png)

- si le livre n'est pas emprunté et le nombre total des livres empruntés et des livres que l'utilisateur a demandé de les emprunter est supérieur ou égal à trois livres, un boutton gris `Emprunter` est affiché. (en cliquant un message d'alert est affiché `Vous avez déja emprunter trois livres.`)

![Capture - 16.png](/img/Capture-16.png)

Si un utilisateur avec un role USER cherche à accéder à des pages d'administration, il sera redirigé automatiquement vers la page d'accueil grâce au code suivant dans le fichier `security.yaml`:
```yaml
access_denied_url: /acceuil
```

# 8- Les pages coté administrateur avec un rôle ADMIN:


L'agent de la bibliothèque doit s'authentifier en tant que **ADMIN**.

Il a le droit:
- de supprimer un livre.

![Capture-48.png](/img/Capture-48.png)

![Capture-49.png](/img/Capture-49.png)

- de modifier un livre.

![Capture-51.png](/img/Capture-51.png)

![Capture-52.png](/img/Capture-52.png)

![Capture-53.png](/img/Capture-53.png)

- ajouter un nouveau Livre.

![Capture-70.png](/img/Capture-70.png)

![Capture-71.png](/img/Capture-71.png)

![Capture-72.png](/img/Capture-72.png)

- de supprimer un éditeur.

![Capture-77.png](/img/Capture-77.png)

![Capture-78.png](/img/Capture-78.png)

- de modifier un éditeur.

![Capture-79.png](/img/Capture-79.png)

![Capture-80.png](/img/Capture-80.png)

![Capture-81.png](/img/Capture-81.png)

- d'ajouter un nouveau éditeur.

![Capture-61.png](/img/Capture-61.png)

![Capture-62.png](/img/Capture-62.png)

![Capture-63.png](/img/Capture-63.png)

- de supprimer un auteur.

![Capture-73.png](/img/Capture-73.png)

![Capture-74.png](/img/Capture-74.png)

- de modifier un auteur.

![Capture-75.png](/img/Capture-75.png)

![Capture-76.png](/img/Capture-76.png)

- d'ajouter un nouveau auteur.

![Capture-67.png](/img/Capture-67.png)

![Capture-68.png](/img/Capture-68.png)

![Capture-69.png](/img/Capture-69.png)

- de supprimer une catégorie.

- de modifier une catégorie.

- d'ajouter une nouvelle catégorie.

![Capture-64.png](/img/Capture-64.png)

![Capture-65.png](/img/Capture-65.png)

![Capture-66.png](/img/Capture-66.png)

