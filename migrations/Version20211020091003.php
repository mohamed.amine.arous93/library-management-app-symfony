<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211020091003 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE livre DROP FOREIGN KEY FK_AC634F9964A082B3');
        $this->addSql('DROP TABLE user_with_livre_to_emprunt');
        $this->addSql('DROP INDEX IDX_AC634F9964A082B3 ON livre');
        $this->addSql('ALTER TABLE livre DROP user_with_livre_to_emprunt_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_with_livre_to_emprunt (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE livre ADD user_with_livre_to_emprunt_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE livre ADD CONSTRAINT FK_AC634F9964A082B3 FOREIGN KEY (user_with_livre_to_emprunt_id) REFERENCES user_with_livre_to_emprunt (id)');
        $this->addSql('CREATE INDEX IDX_AC634F9964A082B3 ON livre (user_with_livre_to_emprunt_id)');
    }
}
