<?php

namespace App\Controller;

use App\Entity\Livre;
use App\Entity\User;
use App\Repository\UserRepository;
use mysql_xdevapi\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/user/show/{id}-{livre}", name="add_livre_to_list", methods={"GET"})
     */
    public function index(User $user, Livre $livre): Response
    {
        $user->addLivreToEmprunt($livre);
        /*$livre->setNbExemplaires($livre->getNbExemplaires() -1);*/
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        return $this->redirectToRoute('livre_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/user/show/{id}", name="show_all_livres_empruntes", methods={"GET"})
     */
    public function showAllLivresEmpruntes(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            /*'controller_name' => 'UserController',*/
            'userrr' => $user,
        ]);
    }

    /**
     * @Route("/user/show/livre-to-emprunt/{id}", name="show_all_livres_to_emprunt", methods={"GET"})
     */
    public function showAllLivresToEmprunt(User $user): Response
    {
        return $this->render('user/show_all_livres_to_emprunt.html.twig', [
            'userrr' => $user,
        ]);
    }

    /**
     * @Route("/admin/allusers", name="show_all_users", methods={"GET"})
     */
    public function showAllUsers(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("user/livre/delete/{id}-{livre}", name="delete_emprunt", methods={"POST"})
     */
    public function delete(Request $request, User $user, Livre $livre): Response
    {
        if ($this->isCsrfTokenValid('delete_emprunt_demand'.$livre->getId(),
            $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $user->removeLivreToEmprunt($livre);
            $entityManager->persist($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('livre_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("user/livre-emprunte/delete/{id}-{livre}", name="delete_livre_emprunte", methods={"POST"})
     */
    public function deleteLivreEmprunte(Request $request, User $user, Livre $livre): Response
    {
        if ($this->isCsrfTokenValid('delete_livre_emprunte'.$livre->getId(),
            $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $user->removeLivresEmpruntes($livre);
            $entityManager->persist($user);
            $livre->setNbExemplaires($livre->getNbExemplaires() + 1);
            $entityManager->persist($livre);
            $entityManager->flush();
        }

        return $this->redirectToRoute('show_all_users', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("admin/livre-to-emprunt/confirm/{id}-{livre}", name="confirm_livre_emprunt_demand", methods={"POST"})
     */
    public function comfirmLivreToEmprunt(Request $request, User $user, Livre $livre): Response
    {
        if ($this->isCsrfTokenValid('confirm_livre_emprunt_demand'.$livre->getId(),
            $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $user->addLivresEmpruntes($livre);
            $user->removeLivreToEmprunt($livre);
            $entityManager->persist($user);
            $livre->setNbExemplaires($livre->getNbExemplaires() - 1);
            $entityManager->persist($livre);
            $entityManager->flush();
        }

        return $this->redirectToRoute('show_all_users', [], Response::HTTP_SEE_OTHER);
    }
}
