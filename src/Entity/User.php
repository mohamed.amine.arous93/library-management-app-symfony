<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use function Sodium\add;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"login"}, message="There is already an account with this login")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $login;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\ManyToMany(targetEntity=Livre::class)
     */
    private $livres_empruntes;

    /**
     * @ORM\ManyToMany(targetEntity=Livre::class)
     * @ORM\JoinTable(name="user_livreToEmprunt")
     */
    private $livreToEmprunt;

    public function __construct()
    {
        $this->livres_empruntes = new ArrayCollection();
        $this->livreToEmprunt = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->login;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Livre[]
     */
    public function getlivres_empruntes(): Collection
    {
        return $this->livres_empruntes;
    }

    public function addLivresEmpruntes(Livre $livre): self
    {
        if (!$this->livres_empruntes->contains($livre)) {
            $this->livres_empruntes[] = $livre;
        }
        /*$this->livres_empruntes[] = $livre;*/
        return $this;
    }

    public function removeLivresEmpruntes(Livre $livre): self
    {
        $this->livres_empruntes->removeElement($livre);

        return $this;
    }

    /**
     * @return Collection|Livre[]
     */
    public function getLivreToEmprunt(): Collection
    {
        return $this->livreToEmprunt;
    }

    public function addLivreToEmprunt(Livre $livreToEmprunt): self
    {
        if (!$this->livreToEmprunt->contains($livreToEmprunt)) {
            $this->livreToEmprunt[] = $livreToEmprunt;
        }

        return $this;
    }

    public function removeLivreToEmprunt(Livre $livreToEmprunt): self
    {
        $this->livreToEmprunt->removeElement($livreToEmprunt);

        return $this;
    }
}
