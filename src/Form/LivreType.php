<?php

namespace App\Form;

use App\Entity\Auteur;
use App\Entity\Categorie;
use App\Entity\Editeur;
use App\Entity\Livre;
use Doctrine\Common\Collections\Expr\Value;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LivreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('nbPages')
            ->add('dateEdition', DateType::class, ['widget' => 'single_text'])
            ->add('nbExemplaires')
            ->add('prix', null, [
                'required'   => false,
                'empty_data' => 0,
            ])
            ->add('isbn', NumberType::class, array(
                'label' => 'isbn ',
                'attr' => array(
                    'placeholder' => 'isbn sur 8 chiffres')))
            ->add('editeur', EntityType::class, ['class' => Editeur::class, 'choice_label' => 'nomEditeur'])
            ->add('category', EntityType::class, ['class' => Categorie::class, 'choice_label' => 'designation'])
            ->add('auteurs', EntityType::class, ['class' => Auteur::class, 'choice_label' => function (Auteur $auteur) {
                return $auteur->getPrenom() . ' ' . $auteur->getNom();
                },'multiple' => true,
                ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Livre::class,
        ]);
    }
}
